package validator

import "fmt"

var constMessageLookup = map[string]string{
	"min":        "Minimum:",
	"max":        "Maximum:",
	"required":   "A value is required",
	"url":        "A valid url must be supplied",
	"len":        "Length must be:",
	"docker_opt": "Invalid docker image format. Required: [namespace/]image[:tag]",
}

func formatErrMessage(field string, value interface{}, tag string, param string) string {
	formattedMessage := defaultErrMsg + field + ":"

	formattedMessage = fmt.Sprintf("%s %s", formattedMessage, constMessageLookup[tag])

	if param != "" {
		formattedMessage = fmt.Sprintf("%s %s", formattedMessage, param)
	}

	if value != "" {
		formattedMessage = fmt.Sprintf("%s, Current: %v", formattedMessage, value)
	}

	return formattedMessage
}
